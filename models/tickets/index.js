/*
   Column    |       Type        | Modifiers | Storage  | Stats target | Description
-------------+-------------------+-----------+----------+--------------+-------------
 id          | character varying | not null  | extended |              |
 plate       | character varying | not null  | extended |              |
 date        | date              | not null  | plain    |              |
 amount      | integer           | not null  | plain    |              |
 description | character varying | not null  | extended |              |
 infraction  | character varying | not null  | extended |              |

Indexes:
    "tickets_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "tickets_plate_fkey" FOREIGN KEY (plate) REFERENCES plates(id)
*/
var db = require('../../db/config');

exports.createTicket = function(values, cb) {
    db.query("INSERT INTO tickets(id, plate, date, infraction, description, amount) VALUES ($1, $2, $3, $4, $5, $6);", values, function(err, res) {
        if (err) return cb(err);
        return cb();
    });
};

exports.getTickets = function(plate, cb) {
    db.query("SELECT * FROM tickets WHERE plate=$1;", [plate], function(err, row) {
        if (err) return cb(err);
        if (row.rowCount === 0 ) {
            return cb(null, null);
        } else {
            return cb(null, row.rows);
        }
    });
};