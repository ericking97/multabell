/*
 Column |       Type        | Modifiers | Storage  | Stats target | Description
--------+-------------------+-----------+----------+--------------+-------------
 id     | character varying | not null  | extended |              |

Indexes:
    "plates_pkey" PRIMARY KEY, btree (id)
Referenced by:
    TABLE "tickets" CONSTRAINT "tickets_plate_fkey" FOREIGN KEY (plate) REFERENCES plates(id)

*/
var db = require('../../db/config');

exports.createPlate = function(value, cb) {
    db.query("INSERT INTO plates VALUES ($1);", [value], function(err, row) {
        if (err) return cb(err);
        return cb(null);
    });
};

exports.getPlates = function(cb) {
    db.query("SELECT * FROM plates;", [], function(err, row) {
        if (err) return cb(err);
        if (row.rowCount === 0 ) {
            return cb(null, null);
        } else {
            return cb(null, row.rows);
        }
    });
}