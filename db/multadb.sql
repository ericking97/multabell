CREATE TABLE "plates" (
    id varchar not null primary key
);

CREATE TABLE "tickets" (
    "id" varchar not null primary key,
    "plate" varchar not null references plates (id),
    "date" date not null,
    "amount" real not null,
    "description" varchar not null,
    "infraction" varchar not null
);
