# ¿Como dar un servicio similar a Multavisa?

0. Crear tests y algun ambiente de pruebas (Jenkins, Gitlab CI, Travis).
1. Migrar todo el frontend a Angular.
2. Crear tabla de usuarios y relacionarla con "plates".
3. Añadir un login y soporte de sesiones al sitio, manteniendo todo el "backend" separado del sitio web.
4. Generar una api usando las rutas existentes, posibilidad de hacer una aplicación en Android y IOs.

Con esos pasos, tendríamos el inicio de una aplicación escalable.

# Integración de placas del Estado y DF.

0. Usar el sitio: https://data.finanzas.cdmx.gob.mx/sma/Consultaciudadana/
1. Desarrollar una forma de resolver el captcha.
2. Usar el sitio: http://infracciones.edomex.gob.mx/
3. Pedirle al usuario los datos necesarios para hacer consultas por él, almacenarlas.
4. Realizar el scrapping en ambos sitios.

