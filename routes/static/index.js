var express = require('express');
var router = express.Router();

var platesModel = require('../../models/plates');
var ticketsModel = require('../../models/tickets');

router.get('/', function(req, res) {
    platesModel.getPlates(function(err, row) {
        if (err) return res.render('error', {"message": "Error", "error": err});
        return res.render('index', {"placas": row, "message": req.query.save});
    });
});

router.get('/multas/:plate', function(req, res) {
    ticketsModel.getTickets(req.params.plate, function(err, row) {
        if (err) return res.render('error', {message: 'Error', error: err});
        return res.render('tickets', {'multas': row});
    });
});

module.exports = router;
