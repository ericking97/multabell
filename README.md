# Multabell

## Requirements
* [Node 6.9](https://nodejs.org/es/)
* [PostgreSQL@9.6.2](https://www.postgresql.org/download/)

Mac OS:
Node:
```
>$ brew install node@6
```
Postgresql:
```
>$ brew install postgresql@9.6
```

Linux:

Node [Guide](https://nodejs.org/es/download/package-manager/#distribuciones-de-linux-basadas-en-debian-y-ubuntu)
Postgresql [Guide](https://www.digitalocean.com/community/tutorials/como-instalar-y-utilizar-postgresql-en-ubuntu-16-04-es)


## Installation
Install node modules by running:
```
>$ npm install
```
Create the database by running:
```
>$ psql -U <your_user> -c "create database multadb;"
>$ psql multadb < <path_to_multadb.sql>
```
NOTE: multadb.sql is located on ./db/

Create your env variables to let node access your database
```
>$ export dbuser=<your_pg_user>
>$ export dbpass=<your_pg_pass>
>$ export dbhost=<your_host/"localhost">
>$ export dbname=<your_db/"multadb">
```
Note: I recommend exporting this vars on your .bash_profile or .zshrc if you want them to be accessible all the time

## Run
```
>$ npm start
```