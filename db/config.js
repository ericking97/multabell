const { Pool } = require('pg');

const pool = new Pool({
    user: process.env.dbuser,
    password: process.env.dbpass,
    host: process.env.dbhost,
    port: 5432,
    database: process.env.dbname
});

module.exports = {
    query: (text, params, callback) => {
        return pool.query(text, params, callback);
    }
}