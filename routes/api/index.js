var express = require('express');
var router = express.Router();

var request = require('request');
var cheerio = require('cheerio');

var platesModel = require('../../models/plates');
var ticketModel = require('../../models/tickets');

var url = require('url');

router.post('/', function(req, res, next) {
    
    const actionUrl = "http://servicios.monterrey.gob.mx/consultas/transito/Reportes/Infracciones.aspx";

    const headParams = {
        "Content-Type": "application/x-www-form-urlencoded"
    };

    const bodyParams = {
        "__EVENTVALIDATION": "/wEWAwKhu6KsDALIrvbuBALE/qnLDmEh3j3X0HyTgOGi4TX7HVDXd+j8",
        "__VIEWSTATE": "/wEPDwUKLTIxMjcwMzAyNmRk4E9fBVzudqbzGsCYlcQbNxEpohM=",
        "__VIEWSTATEGENERATOR": "B256B613",
        "ctl00$ContentPlaceHolder1$btnBuscar": "Buscar",
        "ctl00$ContentPlaceHolder1$txtNumPlaca": req.body.plate
    };

    request.post({headers: headParams, url: actionUrl, form: bodyParams}, function(err, response, body) {
        if (err) res.send(500);
        var $ = cheerio.load(body);

        var data = [];
        var processedData = [];

        $('table#ctl00_ContentPlaceHolder1_tblEdoCuenta td').each(function() {
            var cellText = $(this).html();
            data.push(cellText);
        });

        if (data.length > 6) {
            var chunk = 6;
            for (var i = 6; i < data.length; i+=chunk) {
                processedData.push(data.slice(i, i+6))
            }
            info = {multas: processedData, data: JSON.stringify(processedData)}
            res.render('results', info);
        } else {
            res.render('results');
        }
    });
});

router.post('/save', function(req, res, next) { 
    
    function formatDate (date) {
        const months = {
            "Jan": '01',
            "Feb": "02",
            "Mar": "03",
            "Apr": "04",
            "May": "05",
            "Jun": "06",
            "Jul": "07",
            "Aug": "08",
            "Sep": "09",
            "Oct": "10",
            "Nov": "11",
            "Dec": "12"
        }
        var newDate =  months[date.substring(3, 6)] + '/' + date.substring(0, 3) + date.substring(6, 12);
        return newDate;
    }

    const plate = JSON.parse(req.body.multas)[0][1];

    platesModel.createPlate(plate,  function(err) {
        // If the error is not "duplicate key value", raise error
        if (err && err.code !== '23505') {
            return res.render('error', {"message": "Error", "error": err});
        }
        // If everything goes well, create tickets
        for (i in JSON.parse(req.body.multas)) {
            // Format the info (Comes as string from the form)
            item = JSON.parse(req.body.multas)[i];
            item[2] = formatDate(item[2]);
            item[5] = parseFloat(item[5].substring(1).replace(',', ''));

            // After formatting create the ticket
            ticketModel.createTicket(item, function(err) {
                // If the error is not "duplicate key value", raise error
                if (err && err.code !== '23505') {
                    return res.render('error', {"message": "Error", "error": err});
                }
            });
        }
        // Return to the home page
        return res.redirect('/?save=true');
    });
});

module.exports = router;